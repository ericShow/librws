# librws(鸿蒙Hi_3861开发板的移植版本)

### 项目说明

该库移植自[OlehKulykov/librws: Tiny, cross platform websocket client C library (github.com)](https://github.com/OlehKulykov/librws)

详细说明可到[原仓库](https://github.com/OlehKulykov/librws)查看或者查看[README](README_origin.md)

### 使用方式

将项目clone至//third_party目录下，然后在//vendor/hisi/hi3861/hi3861/BUILD.gn文件中的lite_component("sdk")-->deps下添加 "//third_party/librws:librws_static"：

<img src="https://gitee.com/balaLaa/librws/raw/master/img/png1.png" alt="图片" style="zoom: 67%;" />

编写应用代码时，头文件目录需要包括"//third_party/librws"目录

<img src="https://gitee.com/balaLaa/librws/raw/master/img/png2.png" alt="2" style="zoom:67%;" />

### 示例代码

[librws_example: 在Hi_3861开发板上使用librws库 (gitee.com)](https://gitee.com/balaLaa/librws_example)